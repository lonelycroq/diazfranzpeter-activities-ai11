import React, { Component } from 'react';
import { StyleSheet, Text, SafeAreaView, TextInput, Button  } from 'react-native';

export default class App extends React.Component {

    state = {
        inBox: ''
    }


    textInBox = (text) => this.setState({inBox: text})
    render() {

        return (

            <SafeAreaView style={styles.container}>


                <TextInput style={styles.in}
                    underlineColorAndroid="transparent"
                    placeholder="Type anything here"
                    placeholderTextColor="#9a73ef"
                    autoCapitalize="none"
                    textAlign='center'
                    onChangeText={this.textInBox} />

                <Button title="submit" onPress={() => alert("You just typed: "+this.state.inBox)} />


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
    },
    in: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1,
        width: 200
    }
});

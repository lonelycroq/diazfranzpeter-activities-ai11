import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  Button
 } from 'react-native';

 const NavigateToDetailes = props => {
    props.navigation.navigate("TODO APP");
 }

const Done = props => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Completed Task Go Here</Text>
          <View style={{width: '100%', top:'45.91%'}}>
           <Button title='Go Back to App' onPress={() => NavigateToDetailes(props)}/>
          </View>
        </View>
      );
  }

export default Done
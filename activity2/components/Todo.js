import React, {useState} from "react";
import { 
  View, 
  StyleSheet, 
  Text, 
  ScrollView,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Button
} from "react-native";



const NavigateToDetailes = props => {
  props.navigation.navigate("Completed Task");
}

const Todo = props => {

  const [Task, setTask] = useState()
  const [Tasks, setTasks] = useState([])

  const addTask = (val) => {
    setTask(val)
  }
  const addTasks = () => {
      setTasks([{key: Math.random(), task: Task}, ...Tasks])
      setTask(null)
  }

  const removeItem = (itemKey) => {
    setTasks(Tasks.filter(item => item.key != itemKey));
  }

  return(
    <SafeAreaView style={styles.body}>

      <View style={styles.header}>
        <Text style={styles.title}>To-Do APP</Text>
      </View>

        <View style={styles.addTaskSection}>
          <TextInput 
            style={styles.input}
            placeholder="Add text here..."
            placeholderTextColor={'#d7d9d9'}
            onChangeText={addTask}
            value={Task}/>
          <TouchableOpacity style={styles.button} onPress={addTasks}>
            <Text style={styles.buttonText}>+</Text>
          </TouchableOpacity>
        </View>
        
        <ScrollView style={styles.displayTask}>

          {
            Tasks.map((objectives) => {
              return(
                <View style={styles.taskItem} key={objectives.key}>
            
                <TouchableOpacity style={styles.done} onPress={() => removeItem(objectives.key)}>
                  <Text style={styles.doneText}>✔</Text>
                </TouchableOpacity>
                <Text style={styles.text}>{objectives.task}</Text>

              </View>
              )
            })
          }

        </ScrollView>

        <TouchableOpacity 
        style={styles.navButton}
        onPress={() => NavigateToDetailes(props)}>
          <Text>Go to Completed Task</Text>
        </TouchableOpacity>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: '#000',
    opacity: .9,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  header: {
    height: 'auto',
    width: '100%',
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginTop: 10,
  },
  title: {
    textTransform: 'uppercase',
    fontSize: 60,
    fontWeight: 'bold',
    color: 'orange',
    textShadowColor: '#022af2',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  addTaskSection: {
    margin: 10,
    marginTop: -10,
    height: '20%',
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  input: {
    borderWidth: 2,
    backgroundColor: 'transparent',
    opacity: 0.8,
    width: '77%',
    padding: 8,
    borderRadius: 5,
    fontSize: 18,
    alignItems: 'center',
    color: '#d8e5e8',
    borderColor: '#fa7b20',
  },
  button: {
    borderWidth: 3,
    paddingHorizontal: '4.1%',
    paddingVertical: '1%',
    borderColor: '#031414',
    borderRadius: 360,
    backgroundColor: '#fa7b20',
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 30,
    color: '#fff',
    marginTop: '-10%'
  },
  displayTask: {
    height: '80%',
    width: '90%',
    flexDirection: 'column',
    backgroundColor: 'transparent',
    borderRadius: 10,
    padding: 5,
    backgroundColor: '#242924',
    marginTop: -30,
    },
  taskItem: {
    backgroundColor: 'red',
    margin: 10,
    padding: 10,
    borderColor: '#fa7b20',
    borderWidth: 2,
    backgroundColor: 'transparent',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  text: {
    color: '#fffc',
    fontSize: 20,
    width: '90%',
    marginLeft: '5%',
    opacity: 0.8
  },
  done: {
    borderWidth: 1,
    borderColor: '#d17334',
    paddingHorizontal: '1%',
    borderRadius: 5,
    margin: 5,
    backgroundColor: 'transparent'
  },
  doneText: {
    color: '#00ff2a',
    fontSize: 20,
  },
  navButton: {
    width: '100%',
    height: '8%',
    marginTop: '3%',
    backgroundColor: '#fa7b20',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default Todo;
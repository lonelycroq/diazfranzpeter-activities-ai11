import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Home Component
import Todo from './components/Todo';
//Done Task Component
import Done from './components/Done'

const Stack = createNativeStackNavigator();


function App() {
  return(
      <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="TODO APP" component={Todo} />
        <Stack.Screen name="Completed Task" component={Done} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
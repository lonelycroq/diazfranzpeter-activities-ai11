require("../library/swisscalc.lib.format.js");
require("../library/swisscalc.lib.operator.js");
require("../library/swisscalc.lib.operatorCache.js");
require("../library/swisscalc.lib.shuntingYard.js");
require("../library/swisscalc.display.numericDisplay.js");
require("../library/swisscalc.display.memoryDisplay.js");
require("../library/swisscalc.calc.calculator.js");

import React from 'react';
import { View, useState, StyleSheet, Text, FlatList } from 'react-native';
import { CalculatorButton, CalculatorDisplay } from '../components';


export default class CalculatorScreen extends React.Component {

    constructor(props){
        super(props);

        this.state = {

            display: "0",
            history: [],
            merge: {},
            mergeAllToHistory: '',
            num1: '',
            num2: '',
            operator: '',
            answerHistory: '',
            click: 0,
            alertDone: false
        };

        this.oc = global.swisscalc.lib.operatorCache;
        this.calc = new swisscalc.calc.calculator();
    }

    onPressDigit = (digit) => {
        this.calc.addDigit(digit);
        this.setState({ display : this.calc.getMainDisplay() });
        let displayLength = this.state.display.length
        if(this.state.click>0){
            if(digit!==0){
                this.setState({ num2: this.state.num2.concat(digit) });
            }
        }else if(this.state.click===0){
            this.setState({ num1: this.state.num1.concat(digit) });
        }
    }

    onPressBinaryOperator = (operator) => {
        this.setState({ click: this.state.click+1});
        this.calc.addBinaryOperator(operator);
        this.setState({ display : this.calc.getMainDisplay() });
        if(operator == this.oc.AdditionOperator){
            this.setState({ operator: " + "});
        }else if(operator == this.oc.SubtractionOperator){
            this.setState({ operator: " - "});
        }else if(operator == this.oc.MultiplicationOperator) {
            this.setState({ operator: " x "});
        }else if(operator == this.oc.DivisionOperator){
            this.setState({ operator: " ÷ "});
        }
    }

    onPressUnaryOperator = (operator) => {
        this.calc.addUnaryOperator(operator);
        this.setState({ display : this.calc.getMainDisplay() });
    }

    onPessClear = () => {
        this.calc.clear();
        this.setState({ display : this.calc.getMainDisplay() });
        this.setState({ num1: '',num2: '',operator: '', merge: '' , mergeAllToHistory: '', click: 0 ,answerHisory: ""});
    }

    onPressPlusMinus = () => {
        this.calc.negate();
        this.setState({ display : this.calc.getMainDisplay() });
    }

    onPressEqual = () => {
        this.calc.equalsPressed();
        this.setState({display : this.calc.getMainDisplay(), answerHisory: this.state.display});
        this.setState({ mergeAllToHistory: this.state.num1 + this.state.operator + this.state.num2 + ' = ' + this.calc.getMainDisplay()});
        this.setState(
            {   merge: {history: this.state.mergeAllToHistory},
                history: this.state.history.concat(this.state.merge), 
                alertDone: true
            }
            );
        
        if(this.state.alertDone){
            alert("Click the equal button trice, because there is a fcking bug with the saving process on the history, and I can't fix it yet");
            alert("Click the clear button before you calculate another problem.");
        }
       if(this.state.answerHistory!==''){
        this.setState({ num1: '',num2: '',operator: '', merge: '' , mergeAllToHistory: '', click: 0 ,answerHisory: ""});
       }
    }


    render(){
        
        return(
            <View style={styles.container}>
                <View style={styles.displayContainer}>
                <View style={styles.history}>
                <Text >HISTORY</Text>
                <FlatList 
                    data={this.state.history}
                    renderItem={({ item }) => (
                        <View style={styles.historyContainer}> 
                            <Text style={styles.historyText}>{ item.history }</Text>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
                </View>
                <CalculatorDisplay display={this.state.display} />
                </View>

                    <View style={styles.buttonContainer}> 
                    <View style={styles.buttonRow}>
                        <CalculatorButton onPress={this.onPessClear} title="C" color= '#000' backgroundColor= '#e9e916' />
                        <CalculatorButton onPress={this.onPressPlusMinus}title="+/-" color= '#000' backgroundColor= '#e9e916' />
                        <CalculatorButton onPress ={() => this.onPressUnaryOperator(this.oc.PercentOperator)} title="%" color= '#000' backgroundColor= '#e9e916' />
                        <CalculatorButton onPress={() => {this.onPressBinaryOperator(this.oc.DivisionOperator)} } title="÷" color= '#000' backgroundColor= '#fc4f4f' />
                    </View>

                    <View style={styles.buttonRow}>
                        <CalculatorButton onPress={() => {this.onPressDigit("7")}} title="7" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("8")}} title="8" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("9")}} title="9" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressBinaryOperator(this.oc.MultiplicationOperator)} }title="x" color= '#000' backgroundColor= '#fc4f4f' />
                    </View>

                    <View style={styles.buttonRow}>
                        <CalculatorButton onPress={() => {this.onPressDigit("4")}} title="4" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("5")}} title="5" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("6")}} title="6" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressBinaryOperator(this.oc.SubtractionOperator)} }title="-" color= '#000' backgroundColor= '#fc4f4f' />
                    </View>

                    <View style={styles.buttonRow}>
                        <CalculatorButton onPress={() => {this.onPressDigit("1")}} title="1" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("2")}} title="2" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressDigit("3")}} title="3" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress={() => {this.onPressBinaryOperator(this.oc.AdditionOperator)} }title="+" color= '#000' backgroundColor= '#fc4f4f' />
                    </View>

                    <View style={styles.buttonRow}>
                        <CalculatorButton onPress={() => {this.onPressDigit("0")}}style= {{flex:1.7}} title="0" color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton omPress={() => {this.onPressDigit(".")}} title="." color= '#000' backgroundColor= '#03fcdf' />
                        <CalculatorButton onPress = {this.onPressEqual} title="=" color= '#000' backgroundColor= '#fc4f4f' />
                    </View>
                    </View>  
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#13131a"
     },
    displayContainer: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    buttonContainer: {
        paddingBottom: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    history: {
        flex: 1,
        backgroundColor: '#fff',
        borderWidth: 2,
        borderBottomColor: 'red',
        borderTopColor: 'red',
        marginTop: 40,
    },
    historyText: {
        color: '#000',
        fontSize: 20,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    historyContainer: {
        backgroundColor: '#fff'
    }
});
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import { backgroundColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';

export default class CalculatorButton extends React.Component {


    static defaultProps = {
        onPress: function() { },
            title: "",
            color: "white",
            backgroundColor: "black",
            style: { }, 
    }

    render() {
        var bgColor = this.props.backgroundColor;


        return(
            <TouchableOpacity onPress={this.props.onPress} style={[styles.container, {backgroundColor: bgColor}, {...this.props.style }]}>
                <Text style={[styles.text, {color: this.props.color}]}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    container :{
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        height: 80,
        borderRadius: 40,
        margin: 4,
     },
    text: {
        fontSize: 30,
        fontWeight: 'bold',
     },
}
);
import React, { useState } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, FlatList } from 'react-native';
import { useFonts } from 'expo-font';



const AnsButton = (props) => {

    const [buttonSelected, setbuttonSelected] = useState(null);

    const selected = (digit) => {
        setbuttonSelected(digit);
    }

    const onPress = (digit) => {
        props.onPress(digit);
        selected(digit);
    }

    const [loaded] = useFonts({
        'Jura-Bold': require('../assets/fonts/Jura-Bold.ttf')
    });

    if(!loaded){
        return null;
    }

    return(
        <View style={styles.container}>

            <TouchableHighlight style={(buttonSelected == 1) ? styles.buttonPressed : styles.button} onPress={() => onPress(1)}>
                <Text style={styles.txt}>{props.a}</Text>
            </TouchableHighlight>
            <TouchableHighlight style={(buttonSelected == 2) ? styles.buttonPressed : styles.button} onPress={() => onPress(2)}>
                <Text style={styles.txt}>{props.b}</Text>
            </TouchableHighlight>
            <TouchableHighlight style={(buttonSelected == 3) ? styles.buttonPressed : styles.button} onPress={() => onPress(3)}>
                <Text style={styles.txt}>{props.c}</Text>
            </TouchableHighlight>
            <TouchableHighlight style={(buttonSelected == 4) ? styles.buttonPressed : styles.button} onPress={() => onPress(4)}>
                <Text style={styles.txt}>{props.d}</Text>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1.5,
        width: '100%',
        backgroundColor: '#000',
        borderTopEndRadius: 50,
        borderTopLeftRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        width: '80%',
        borderWidth: 1,
        borderColor: '#828182',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        padding: '3%',
        margin: '2%'
    },
    buttonPressed: {
        width: '80%',
        borderWidth: 1,
        borderColor: '#ff0000',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        padding: '3%',
        margin: '2%'
    },
    txt: {
        color: '#e0e0e0',
        fontSize: 18,
        fontFamily: 'Jura-Bold'
    }
});

export default AnsButton;
import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { useFonts } from 'expo-font';

const ReturnButton = (props) => {

    const[loaded] = useFonts({
        'Goldman-Regular': require('../assets/fonts/Goldman-Regular.ttf')
    });

    if(!loaded){
        return null;
    }

    return(
        <TouchableOpacity style={styles.button} onPress={props.onPress}>
            <Text style={styles.buttonText}>Return Home</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        padding: '3%',
        borderRadius: 120,
        backgroundColor: '#33004d',
        elevation: 5
      },
      buttonText: {
        color: '#fff',
        fontSize: 26,
        fontFamily: 'Goldman-Regular', 
        marginBottom: '-2%'
      }
});

export default ReturnButton;
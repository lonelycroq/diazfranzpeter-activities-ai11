import React, {useState} from 'react';
import {View, Text} from 'react-native';

export default function History(){

    const [history, setHistory] = useState([{}]);
    return(
        <View>
            <Text>History</Text>
            {
                history.map((item)=>{
                    return(
                        <View style={styles.items} key={Math.random}>
                            <Text>{item.history.}</Text>
                        </View>
                    )
                })
            }
        </View>
    )
}
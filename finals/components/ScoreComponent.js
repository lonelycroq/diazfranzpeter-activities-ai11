import * as React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { useFonts } from 'expo-font';


const ScoreComponent = (props) => {

    var FinalScore = props.FinalScore;

    const [loaded] = useFonts({
        'SansBold': require('../assets/fonts/SansBold.ttf'),
        'Rajdhani-Regular': require('../assets/fonts/Rajdhani-Regular.ttf'),
        'Play': require('../assets/fonts/Play-Regular.ttf'),
        'BebasNeue-Regular': require('../assets/fonts/BebasNeue-Regular.ttf')
    })

    if(!loaded) {
        return null
    }

    return(
        <View style={styles.container}>
            <Text style={styles.headerText}>Congrats!</Text>
            <Image style={{width: '90%', height: '55%', marginTop: '-25%'}} resizeMode='stretch' source={require('../assets/trophy.png')}/>
            <Text style={styles.message}>Quiz Completed Succesfuly</Text>
            <Text style={styles.Scoreheader}>YOUR SCORE</Text>
            <Text style={styles.score}>{FinalScore}/10</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '85%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#faf9f5',
        borderRadius: 40,
        elevation: 5,
        shadowColor: '#ff99ff',
        padding: '5%'
    },
    headerText: {
        color: '#000',
        fontSize: 30,
        fontFamily: 'SansBold'
    },
    Scoreheader: {
        fontSize: 30,
        marginBottom: '-20%',
        fontFamily: 'Rajdhani-Regular'
    },
    score: {
        fontSize: 45,
        fontFamily: 'Play'
    },
    message: {
        fontSize: 30,
        marginTop: '-15%',
        fontFamily: 'BebasNeue-Regular'
    }
});

export default ScoreComponent;
import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useFonts } from 'expo-font';




const Question = (props) => {

    const [loaded] = useFonts({
        'Play-Regular': require('../assets/fonts/Play-Regular.ttf'),
        'SansCondensed': require('../assets/fonts/SansCondensed.ttf')
      });

      if (!loaded) {
        return null;
      }

    return(
        <View style={styles.qustionContainer}>
            <View>
                <Text style={{color: '#c5c1c7', fontSize: 35, fontFamily: 'SansCondensed'}}>Question {props.questionNo}/10</Text>
                <Text style={{color: '#c5c1c7', fontSize: 25}}>- - - - - - - - - - - - - - - - - - - - - - - -</Text>
            </View>
            <Text style={styles.txt}>{props.question}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    qustionContainer: {
        padding: '10%',
        backgroundColor: '#000',
        flex: 1,
    },
    txt: {
        fontSize: 25,
        color: '#e0e0e0',
        fontFamily: 'Play-Regular'
    },
})

export default Question;
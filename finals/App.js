
import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//import Screens
import HomeScreen from './screens/HomeScreen';
import QuestionScreenOne from './screens/QuestionScreenOne'
import QuestionScreenTwo from './screens/QuestionScreenTwo'
import QuestionScreenThree from './screens/QuestionScreenThree';
import QuestionScreenFour from './screens/QuestionScreenFour';
import QuestionScreenFive from './screens/QuestionScreenFive';
import QuestionScreenSix from './screens/QuestionScreenSix';
import QuestionScreenSeven from './screens/QuestionScreenSeven';
import QuestionScreenEight from './screens/QuestionScreenEight';
import QuestionScreenNine from './screens/QuestionScreenNine';
import QuestionScreenTen from './screens/QuestionScreenTen';
import Result from './screens/ResultScreen';


const Stack = createNativeStackNavigator();

const App = () => {
  return(
      <NavigationContainer>
      <Stack.Navigator initialRouteName='Homne'>
        <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false, animation: 'slide_from_right' }}/>
        <Stack.Screen name="QScreenOne" component={QuestionScreenOne} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenTwo" component={QuestionScreenTwo} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenThree" component={QuestionScreenThree} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenFour" component={QuestionScreenFour} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenFive" component={QuestionScreenFive} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenSix" component={QuestionScreenSix} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenSeven" component={QuestionScreenSeven} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenEight" component={QuestionScreenEight} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenNine" component={QuestionScreenNine} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="QScreenTen" component={QuestionScreenTen} options={{headerShown: false, animation: 'slide_from_right'}}/>
        <Stack.Screen name="ResultScreen" component={Result} options={{headerShown: false, animation: 'simple_push'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
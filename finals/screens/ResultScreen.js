import React from 'react';
import { View, Text, StyleSheet, StatusBar, ImageBackground } from 'react-native';

import ScoreComponent from '../components/ScoreComponent';
import ReturnButton from '../components/ReturnButton';

const Result = ({navigation, route}) => {

   const {FinalScore} = route.params;
   
   const returnHome = () => {
    navigation.navigate('Home');
   }

    return(
    <ImageBackground resizeMethod='auto' resizeMode='cover' source={require('../assets/confetti.jpg')} style={styles.container}>
        <ScoreComponent FinalScore={FinalScore}/>
        <ReturnButton onPress={returnHome}/>
        <StatusBar backgroundColor={'#000'} barStyle={'light-content'}/>
    </ImageBackground>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    
});

export default Result;
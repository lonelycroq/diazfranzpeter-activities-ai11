import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';

//import components
import Question from '../components/Question'
import AnsButton from '../components/AnsButton'

export default function QuestionScreenNine({navigation, route}){

    const [score, setScore] = useState(0);
    const [nxt, setNxt] = useState(false);
    const { Score8 } = route.params;

    const addScore = (digit) => {
        digit == 3 ? setScore(1 + Score8): setScore(Score8); 
        setNxt(true);
    }

    const navigateToScreen = () => {
        navigation.navigate("QScreenTen", { Score9: score})
    }

    return(
        <View style={styles.container}>
            <Question questionNo = "09" question = "A memory location that holds a single letter or number is called" />
            <AnsButton a={"Double"} b={"Int"} c={"Char"} d={"String"} onPress={addScore}/>
            <TouchableOpacity onPress={navigateToScreen} style={ nxt ? styles.nextButton : styles.disableButton } disabled={!nxt}>
                <Text style={styles.txtButton}>Next</Text>
            </TouchableOpacity>
            <StatusBar backgroundColor={'black'}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
    },
    nextButton: {
        width: '60%',
        backgroundColor: '#33004d',
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    disableButton: {
        width: '60%',
        backgroundColor: '#c2c2c2',
        hieght: '10%,',
        alignItems: 'center',
        opacity: 0.4,
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    txtButton: {
        color: '#fff',
        fontSize: 20
    }
})
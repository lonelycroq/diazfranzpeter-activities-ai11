import * as React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Text } from 'react-native';
import { useFonts } from 'expo-font'



export default function HomeScreen({navigation}) {

  const [loaded] = useFonts({
    'Goldman-Regular': require('../assets/fonts/Goldman-Regular.ttf')
  });

  if(!loaded){
      return null;
  }


  return (
    <View style={styles.container}>
      <Image source={require('../assets/logo.png')} resizeMode={'contain'} style={{height: '40%', width: '80%'}} />
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("QScreenOne")}>
        <Text style={styles.buttonText}>Play Now</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#000',
    paddingTop: '45%',
  },
  button: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '50%',
    borderWidth: 2,
    padding: '3%',
    borderRadius: 120,
    backgroundColor: '#33004d',
  },
  buttonText: {
    color: '#fff',
    fontSize: 26,
    fontFamily: 'Goldman-Regular',
    marginBottom: '-4%',
  }
})

import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';

//import components
import Question from '../components/Question'
import AnsButton from '../components/AnsButton'

export default function QuestionScreenThree({navigation, route}){

    const [score, setScore] = useState(0);
    const [nxt, setNxt] = useState(false);
    const { Score2 } = route.params;

    const addScore = (digit) => {
        digit == 4 ? setScore(1 + Score2): setScore(Score2); 
        setNxt(true);
    }

    const navigateToScreen = () => {
        navigation.navigate("QScreenFour", { Score3: score})
    }

    return(
        <View style={styles.container}>
            <Question questionNo = "03" question = "__________ are containers for storing data values." />
            <AnsButton a={"Array"} b={"Function"} c={"Object"} d={"Variables"} onPress={addScore}/>
            <TouchableOpacity onPress={navigateToScreen} style={ nxt ? styles.nextButton : styles.disableButton } disabled={!nxt}>
                <Text style={styles.txtButton}>Next</Text>
            </TouchableOpacity>
            <StatusBar backgroundColor={'black'}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
    },
    nextButton: {
        width: '60%',
        backgroundColor: '#33004d',
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    disableButton: {
        width: '60%',
        backgroundColor: '#c2c2c2',
        opacity: 0.4,
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    txtButton: {
        color: '#fff',
        fontSize: 20
    }
})
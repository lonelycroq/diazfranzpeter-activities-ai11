import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';

//import components
import Question from '../components/Question'
import AnsButton from '../components/AnsButton'

export default function QuestionScreenSix({navigation, route}){

    const [score, setScore] = useState(0);
    const [nxt, setNxt] = useState(false);
    const { Score5 } = route.params;

    const addScore = (digit) => {
        digit == 2 ? setScore(1 + Score5): setScore(Score5); 
        setNxt(true);
    }

    const navigateToScreen = () => {
        navigation.navigate("QScreenSeven", { Score6: score})
    }

    return(
        <View style={styles.container}>
            <Question questionNo = "06" question = "A loop statement inside another loop statement is called" />
            <AnsButton a={"Loop in loop"} b={"Nested Loop"} c={"Double Loop"} d={"Child Loop"} onPress={addScore}/>
            <TouchableOpacity onPress={navigateToScreen} style={ nxt ? styles.nextButton : styles.disableButton } disabled={!nxt}>
                <Text style={styles.txtButton}>Next</Text>
            </TouchableOpacity>
            <StatusBar backgroundColor={'black'}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
    },
    nextButton: {
        width: '60%',
        backgroundColor: '#33004d',
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    disableButton: {
        width: '60%',
        backgroundColor: '#c2c2c2',
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.4,
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    txtButton: {
        color: '#fff',
        fontSize: 20
    }
})
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';

//import components
import Question from '../components/Question'
import AnsButton from '../components/AnsButton'

export default function QuestionScreenTen({navigation, route}){

    const [score, setScore] = useState(0);
    const [nxt, setNxt] = useState(false);
    const { Score9 } = route.params;

    const addScore = (digit) => {
        digit == 1 ? setScore(1 + Score9): setScore(Score9); 
        setNxt(true);
    }

    const navigateToScreen = () => {
        navigation.navigate("ResultScreen", { FinalScore: score})
    }

    return(
        <View style={styles.container}>
            <Question questionNo = "10" question = "Process of finding and fixing errors in the syntax so the computer program will run properly." />
            <AnsButton a={"Debugging"} b={"Bug"} c={"Command"} d={"Sequence"} onPress={addScore}/>
            <TouchableOpacity onPress={navigateToScreen}
                                style={ nxt ? styles.nextButton : styles.disableButton } 
                                disabled={!nxt}>
                <Text style={styles.txtButton}>Next</Text>
            </TouchableOpacity>
            <StatusBar backgroundColor={'black'}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
    },
    nextButton: {
        width: '60%',
        backgroundColor: '#33004d',
        hieght: '10%,',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    disableButton: {
        width: '60%',
        backgroundColor: '#c2c2c2',
        hieght: '10%,',
        opacity: 0.4,
        alignItems: 'center',
        justifyContent: 'center',
        padding: '3%',
        borderRadius: 120,
        marginBottom: '5%'
    },
    txtButton: {
        color: '#fff',
        fontSize: 20
    }
})
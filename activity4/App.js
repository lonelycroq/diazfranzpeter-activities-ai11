// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Home Screen
import HomeScreen from './src/screens/HomeScreen.js';
//First Page Screen
import Page1 from './src/screens/Page1.js';
//Second Page Screen
import Page2 from './src/screens/Page2.js';
//Third Page Screen
import Page3 from './src/screens/Page3.js'
//Fourth Page Screen
import Page4 from './src/screens/Page4.js'

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Home' component={HomeScreen}
                      options={{
                        headerTransparent: true,
                        headerTintColor: '#000',
                        headerTitleAlign: 'center',
                        title: ' '
        }}/>
        <Stack.Screen name="Page1" component={Page1} options={{
                    headerTransparent: true,
                    headerTintColor: '#fff',
                    headerTitleAlign: 'center',
                    title: ' '
        }}/>
        <Stack.Screen name="Page2" component={Page2} options={{
                    headerTransparent: true,
                    headerTintColor: '#000',
                    headerTitleAlign: 'center',
                    title: null
        }}/>
        <Stack.Screen name="Page3" component={Page3} options={{
                    title: null,
                    headerTransparent: true,
                    headerTitleAlign: 'center',
                    headerTintColor: '#fff'
        }}/>
        <Stack.Screen name="Page4" component={Page4} options={{
          title: null,
          headerTransparent: true,
          headerTitleAlign: 'center',
          headerTintColor: '#fff'
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
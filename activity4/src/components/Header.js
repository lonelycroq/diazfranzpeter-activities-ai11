import React from 'react';
import { 
    View,
    Text,
    StyleSheet,
 } from 'react-native';

 const Header = () => {
     return(
         <View style={styles.header}>
             <Text style={styles.h1}>Reasons why I choose Information Technology</Text>
         </View>
     );
 }

 const styles = StyleSheet.create({
     header: {
         width: '100%',
         height: '25%',
         backgroundColor: 'transparent',
         alignItems: 'flex-start',
         margin: '2.5%',
         top: '4%',
     },
     h1: {
        color: '#fff',
        fontSize: 31,
        fontWeight: 'bold',
        fontFamily: 'sans-serif-condensed',
        textTransform: 'uppercase'
     }
 });

export default Header;
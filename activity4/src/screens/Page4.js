import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
  View,
  Image,
  SafeAreaView
 } from 'react-native';

 import SharedWorkspace from '../../assets/page4.png'
 import BG from '../../assets/BG.jpg'

const NavigateToDetails = props => {
    props.navigation.navigate("Home");
  }

 const Page4 = props => {
   return(
    <ImageBackground source={BG} style={styles.body}>
      <ImageBackground 
      source = {SharedWorkspace} 
      style={styles.Container}
      resizeMode= 'contain'>
        <View style={styles.message}> 
          <Text style={styles.messageText}>I like to learn new things and I love programming.</Text>
        </View>
      </ImageBackground>
      <TouchableOpacity style={styles.button} onPress={() => NavigateToDetails(props)}>
                <Text style={styles.buttonText}>Home</Text>
            </TouchableOpacity>
    </ImageBackground>
   );
 }

 const styles=StyleSheet.create({
  body: {
    height: '100%',
    width:'100%',
    alignItems: 'center'
  },
  Container: {
    height: '90%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
      alignItems: 'center',
      width: '100%',
      top: '25%'
  },
  messageText: {
      fontSize: 21,
      color: '#000',
      fontStyle: 'normal',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      margin: '10%'
  },
  button: {
      height: '6%',
      width: '60%',
      backgroundColor: '#1821c9',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      padding: 5,
      bottom: '1.5%'
  },
  buttonText: {
      fontSize: 20,
      color: '#fff', 
  },
footer: {
    height: '60%',
    alignItems: 'center',
    marginTop: '-21%',
    justifyContent: 'flex-start'
}
 });

 export default Page4;
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
  View,
  Button
 } from 'react-native';

import CareerProgress from '../../assets/careerProgress.png'
import StarBackground from '../../assets/starBackground.jpg'

const NavigateToDetails = props => {
    props.navigation.navigate("Page2");
  }

 const Page1 = props => {
   return(
    <ImageBackground 
    source={StarBackground} 
    style={styles.body} 
    imageStyle= 
    {{opacity:0.999}}>
        <View>
            <ImageBackground 
            source ={CareerProgress} 
            style={styles.Container}
            imageStyle={{
                resizeMode: 'cover',
                marginTop: '10%'
            }}>
                <StatusBar />
            
            </ImageBackground>
        </View>
      <View style={styles.footer}>
        <View style={styles.message}>
                <Text style={styles.messageText}>I choose Information Technology because I want to be successful in life</Text>
            </View>
            
            <TouchableOpacity style={styles.button} onPress={() => NavigateToDetails(props)}>
                <Text style={styles.buttonText}>Continue</Text>
            </TouchableOpacity>
      </View>

    </ImageBackground>
   );
 }

 const styles=StyleSheet.create({
  body: {
    flex: 1,
  },
  Container: {
    width: '100%',
    height: '80%',
    top: '10%',
  },
  text: {
    color: 'black',
    fontSize: 40,
    fontWeight: '500'
  },
  footer: {
    height: '60%',
    alignItems: 'center',
    marginTop: '-20%',
    justifyContent: 'flex-start'
},
  message: {
      alignItems: 'center',
      marginBottom: '10%',
      width: '80%',
  },
  messageText: {
      fontSize: 21,
      color: '#f0f0f2',
      fontStyle: 'normal',
      fontWeight: 'bold',
      textTransform: 'uppercase'
  },
  button: {
      height: '10%',
      width: '60%',
      backgroundColor: '#1821c9',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      padding: 5,
      marginTop: '3%'
  },
  buttonText: {
      fontSize: 20,
      color: '#fff', 
  },

 });

 export default Page1;
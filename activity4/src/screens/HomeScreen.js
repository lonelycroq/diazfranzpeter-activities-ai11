import React, { StatusBar } from 'react';
import { 
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    SafeAreaView,
    Image,
    ImageBackground
 } from 'react-native';

import Header from '../components/Header.js';
import HomeImage from '../../assets/programming.png';
import BgImage from '../../assets/plainBgImage.jpg';

const NavigateToDetails = (props) => {
    props.navigation.navigate('Page1')
}

const HomeScreen = props => {
    return(
        <SafeAreaView style={styles.body}>
            <Header />
            <View style={styles.container}>
                <ImageBackground 
                    source={BgImage} 
                    style={styles.imageBG}
                    resizeMode='contain'>
                    <Image source={HomeImage}
                    style={styles.image}
                    resizeMode='center' />
                    <View style={styles.h2}>
                    <Text style={styles.h2Text}>Want to know Why?</Text>
                </View>
                </ImageBackground>
                
                <TouchableHighlight style={styles.button} onPress={() =>NavigateToDetails(props)}>
                <Text style={styles.buttonText}>Continue</Text>
            </TouchableHighlight>
            </View>
            
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: '#001f4d',
        padding: '8%',
    },
    container: {
        backgroundColor: '#dbdae0',
        borderRadius: 25,
        flexDirection: 'column',
        alignContent: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#001f4d',
    },
    imageBG: {
        width: '100%',
        height: '67%',
        borderRadius: 30
    },
    image: {
        width: '100%',
        height: '70%',
        top:'-5%',
    },
   h2: {
       flex:.5,
       alignItems: 'center',
       justifyContent: 'center'
   },
   h2Text: {
        color: '#000',
        fontSize: 25,
        textTransform: 'uppercase',
        fontWeight: 'bold',
   },
   button: {
       backgroundColor: '#1821c9',
        alignItems: 'center',
        borderRadius: 40,
        width: '60%',
        padding: '4%',
        marginHorizontal: '20%',
        marginTop: '-9%',
   },
   buttonText: {
       color: '#fff',
       textTransform: 'uppercase',
       fontSize: 13,
   }
});

export default HomeScreen;
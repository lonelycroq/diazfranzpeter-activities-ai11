import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
  View,
  Image,
  SafeAreaView
 } from 'react-native';

 import SharedWorkspace from '../../assets/page3.png'
 import BG from '../../assets/starBackground.jpg'

const NavigateToDetails = props => {
    props.navigation.navigate("Page4");
  }

 const Page3 = props => {
   return(
    <ImageBackground source={BG} style={styles.body}>
      <ImageBackground 
      source = {SharedWorkspace} 
      style={styles.Container}
      resizeMode= 'contain'>
        <View style={styles.message}> 
          <Text style={styles.messageText}>Creating Application is one of my dream, so I choose Information Technology.</Text>
        </View>
        <TouchableOpacity style={styles.button} onPress={() => NavigateToDetails(props)}>
                <Text style={styles.buttonText}>Continue</Text>
            </TouchableOpacity>
      </ImageBackground>
    </ImageBackground>
   );
 }

 const styles=StyleSheet.create({
  body: {
    height: '100%',
    width:'100%',
  },
  Container: {
    height: '90%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
      alignItems: 'center',
      width: '100%',
      bottom: '-34%',
  },
  messageText: {
      fontSize: 21,
      color: '#fff',
      fontStyle: 'normal',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      margin: '5%'
  },
  button: {
      height: '6%',
      width: '60%',
      backgroundColor: '#1821c9',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      padding: 5,
      bottom: '-40%'
  },
  buttonText: {
      fontSize: 20,
      color: '#fff', 
  },
footer: {
    height: '60%',
    alignItems: 'center',
    marginTop: '-21%',
    justifyContent: 'flex-start'
}
 });

 export default Page3;
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { 
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
  View,
  Image,
  SafeAreaView
 } from 'react-native';

 import SharedWorkspace from '../../assets/sharedWorkspace.jpg'

const NavigateToDetails = props => {
    props.navigation.navigate("Page3");
  }

 const Page2 = props => {
   return(
    <SafeAreaView>
      <ImageBackground 
      source = {SharedWorkspace} 
      style={styles.Container}
      resizeMode= 'cover'>
        <View style={styles.message}> 
          <Text style={styles.messageText}>I want to work and develop things with other people.</Text>
        </View>
        <TouchableOpacity style={styles.button} onPress={() => NavigateToDetails(props)}>
                <Text style={styles.buttonText}>Continue</Text>
            </TouchableOpacity>
      </ImageBackground>
    </SafeAreaView>
   );
 }

 const styles=StyleSheet.create({
  body: {
    flex: 1,
    padding: '3%',
  },
  Container: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  message: {
      alignItems: 'center',
      width: '100%',
      marginTop: '70%',
  },
  messageText: {
      fontSize: 21,
      color: '#000',
      fontStyle: 'normal',
      fontWeight: 'bold',
      textTransform: 'uppercase'
  },
  button: {
      height: '6%',
      width: '60%',
      backgroundColor: '#1821c9',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      padding: 5,
      bottom: '-20%'
  },
  buttonText: {
      fontSize: 20,
      color: '#fff', 
  },
footer: {
    height: '60%',
    alignItems: 'center',
    marginTop: '-21%',
    justifyContent: 'flex-start'
}
 });

 export default Page2;